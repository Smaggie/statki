import unittest
from plansza import Mapa, Statki, Plansza


class Test_Statki(unittest.TestCase):

    def test_los_statkow (self):
        mapa = Mapa()
        mapa.statki.losuj_statki()

        dlugosc = len(mapa.statki.statki)
        self.assertEqual (dlugosc, 15)

        i = 5
        k = 0
        for x in range (1,6):
            for y in range (x):
                dlugosc = mapa.statki.statki[k].dlugosc
                self.assertEqual (dlugosc, i)
                k += 1
            i -= 1

    def test_akcja (self):
        plansza = Plansza()
        plansza.runda_wroga()
        pole = plansza.pole_wroga
        traf = False
        statki_gracza = plansza.mapa_gracza.statki.statki
        for statek in statki_gracza:
            for p in statek.pola:
                if p == pole:
                    traf = True
        if traf:
            self.assertEqual (plansza.mapa_gracza.mapa[pole], "X")
        else:
            self.assertEqual (plansza.mapa_gracza.mapa[pole], "+")

if __name__ == '__main__':
    unittest.main()
