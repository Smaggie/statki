import random
import os


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


class Statek:
    def __init__ (self, pola):
        self.pola = list(pola)
        self.dlugosc = len(pola)
        self.zatopiony = list(pola)

    def losuj_pole (self, pola):
        p = random.choice(pola)
        x = Statki.X.index(p[0])
        y = Statki.Y.index(p[1:])
        plus = [[x+1,y],[x-1,y],[x,y+1],[x,y-1]]
        while True:
            nowe = random.choice(plus)
            if nowe[0]>-1 and nowe[0]<12:
                if nowe[1]>-1 and nowe[1]<12:
                    p = [Statki.X[nowe[0]], Statki.Y[nowe[1]]]
                    break
        return p


class Statki (Statek):
    X = list("ABCDEFGHIJKL")
    Y = ["1","2","3","4","5","6","7","8","9","10","11","12"]

    def __init__ (self, mapa):
        self.mapa = mapa
        self.statki = []
        self.suma_statkow = 0

    def losuj_statki (self):
        i = 5
        for x in range (1,6):
            for y in range (x):
                self.statki.append(self.losuj_statek(i))
            i -= 1

    def losuj_statek (self, dlugosc):
        while True:
            x = random.choice(self.X)
            y = random.choice(self.Y)
            wolne_pola = self.sprawdz (self.X.index(x), self.Y.index(y))
            if wolne_pola: break
        xy = x+y
        pola = [xy]
        k = 0
        for i in range (dlugosc-1):
            while True:
                p = self.losuj_pole(pola)
                wolne_pola = self.sprawdz (self.X.index(p[0]), self.Y.index(p[1]))
                k += 1
                if k>10000:
                    raise Exception ("Blad losowania - statki zostaly zle rozmieszczone na mapie")
                if wolne_pola:
                    p = p[0]+p[1]
                    if p not in pola:
                        pola.append(p)
                        break
        statek = Statek(pola)
        for p in statek.pola:
            self.mapa[p] = "O"
        self.obrysuj(statek)
        return statek

    def sprawdz (self, x, y):
        for a in range (x-1,x+2):
            if a>-1 and a<12:
                for b in range (y-1,y+2):
                    if b>-1 and b<12:
                        ab = self.X[a]+self.Y[b]
                        if self.mapa[ab] == "O":
                            return False
        return True

    def obrysuj (self, statek):
        lista = []
        for p in statek.pola:
            x = self.X.index(p[0])
            y = self.Y.index(p[1:])
            for a in range (x-1,x+2):
                if a>-1 and a<12:
                    for b in range (y-1,y+2):
                        if b>-1 and b<12:
                            lista.append (self.X[a]+self.Y[b])
        self.nowa = list(set(lista))
        for p in statek.pola:
            for item in self.nowa:
                if p == item:
                    self.nowa.remove(item)
        for i in self.nowa:
            self.mapa[i] = "+"


class Mapa (Statki):
    rozmiar = 12

    def __init__ (self):
        self.klucze = []
        for x in Statki.X:
            for y in Statki.Y:
                self.klucze.append(x+y)
        self.mapa = dict.fromkeys(self.klucze, " ")
        self.statki = Statki(self.mapa)

    def ustaw_statki(self):
        self.statki.losuj_statki()
        for x in Statki.X:
            for y in Statki.Y:
                if self.mapa[x+y] == "+":
                    self.mapa[x+y] = " "
        self.statki.suma_statkow = sum(s.dlugosc for s in self.statki.statki)
        return self.statki.suma_statkow

    def testuj_statki (self):
        s1 = ['A10','A11','B11','B12','C12']
        s2 = ['G2','H2','I2','H1']
        s3 = ['F8','F7','G7','G6']
        s4 = ['C4','C5','C6']
        s5 = ['L5','K5','K6']
        s6 = ['L8','L9','K9']
        s7 = ['G11','G12']
        s8 = ['A8','B8']
        s9 = ['A2','B2']
        s10 = ['D1','D2']
        s11 = ['A5']
        s12 = ['D10']
        s13 = ['K1']
        s14 = ['H4']
        s15 = ['J12']
        for i in range (1,16):
            exec("self.statki.statki.append(Statek(s{}))".format(i))
        self.suma_statkow = sum (s.dlugosc for s in self.statki.statki)
        for statek in self.statki.statki:
            for p in statek.pola:
                self.mapa[p] = "O"
        return self.statki.suma_statkow

    def wczytaj_rzad (self, rzad, wrog):
        string = ""
        for x in Statki.X:
            pole = self.mapa [x + str(rzad)]
            if wrog and pole == "O":
                pole = " "
            string = string + " " + pole + " |"
        return string

    def czy_zatopiony (self, pole):
        for statek in self.statki.statki:
            for p in statek.pola:
                if pole == p:
                    statek.zatopiony.remove(pole)
                    if statek.zatopiony == []:
                        self.obrysuj(statek)
                        return True
        return False

    def usun_plusy (self, klucze_gracza):
        for i in self.nowa:
            if i in klucze_gracza:
                klucze_gracza.remove(i)


class Plansza ():

    def __init__ (self):
        self.mapa_gracza = Mapa()
        self.mapa_wroga = Mapa()
        self.statki_gracza = self.mapa_gracza.ustaw_statki()
        self.statki_wroga = self.mapa_wroga.ustaw_statki()
        self.klucze_gracza = self.mapa_gracza.klucze
        self.klucze_wroga = self.mapa_wroga.klucze
        self.znak_wroga = ""
        self.statek_gracza = Statek([])

    def narysuj_plansze (self):
        print
        napis_gracza = "                    STATKI GRACZA               "
        napis_wroga = "                     STATKI PRZECIWNIKA  "
        print napis_gracza, napis_wroga
        print
        litery = "      "
#        litery = "      A   B   C   D   E   F   G   H   I   J   K   L  "
        for i in range (65,65+Mapa.rozmiar):
            litery = litery + "{}   ".format(chr(i))
        print litery, litery
        znaki = " ---+" + Mapa.rozmiar*"---+"
        print znaki+"  "+znaki
        for i in range (1, Mapa.rozmiar+1):
            print ("  {: <2}|".format(i) + self.mapa_gracza.wczytaj_rzad(i,False) +
                   "    {: <2}|".format(i) + self.mapa_wroga.wczytaj_rzad(i,True))
            print znaki+"  "+znaki

    def runda_gracza (self):
        wrog = self.mapa_wroga.mapa
        while True:
            print
            self.pole_gracza = raw_input ("  Podaj wspolrzedne strzalu: ")
            self.pole_gracza = self.pole_gracza.upper()
            if self.pole_gracza not in self.klucze_wroga:
                print "  nieprawidlowa wartosc"
            elif wrog[self.pole_gracza] == " " or wrog[self.pole_gracza] == "O":
                znak = wrog[self.pole_gracza]
                self.znak_gracza = znak
                self.akcja (znak, "gracz")
                if znak == "O":
                    self.statki_wroga -= 1
                break
            else:
                print "  wybierz inne pole"

    def runda_wroga (self):
        gracz = self.mapa_gracza.mapa
        pola = self.statek_gracza.pola
        if self.statek_gracza.pola != []:
            while True:
                pole = self.statek_gracza.losuj_pole(pola)
                self.pole_wroga = pole[0]+pole[1]
                if self.pole_wroga in self.klucze_gracza: break
        else:
            self.pole_wroga = random.choice (self.klucze_gracza)
        znak = gracz[self.pole_wroga]
        self.znak_wroga = znak
        if znak == " " or znak =="O":
            if znak == "O":
                self.statki_gracza -= 1
                self.statek_gracza.pola.append(self.pole_wroga)
            self.akcja (znak, "wrog")
        self.klucze_gracza.remove(self.pole_wroga)

    def akcja (self, znak, kto):
        gracz = self.mapa_gracza
        wrog = self.mapa_wroga
        if znak == "O": znak = "X"
        elif znak == " ": znak = "+"
        if kto == "gracz":
            wrog.mapa [self.pole_gracza] = znak
            self.zatop_wrog = wrog.czy_zatopiony(self.pole_gracza)
        elif kto == "wrog":
            gracz.mapa [self.pole_wroga] = znak
            self.zatop_gracz = gracz.czy_zatopiony(self.pole_wroga)
            if self.zatop_gracz:
                gracz.usun_plusy (self.klucze_gracza)
                self.statek_gracza.pola = []

    def napis (self, kto):
        if kto == "gracz":
            pole = self.pole_gracza
            zatop = self.zatop_wrog
            znak = self.znak_gracza
        elif kto == "wrog":
            pole = self.pole_wroga
            zatop = self.zatop_gracz
            znak = self.znak_wroga
        if znak == " ":
            print "  wybrano pole: %s" % pole, "PUDLO"
        elif znak == "O":
            if zatop:
                print "  wybrano pole: %s" % pole, "TRAFIONY ZATOPIONY"
            else:
                print "  wybrano pole: %s" % pole, "TRAFIONY"

    def komunikat (self):
        cls()
        self.narysuj_plansze()
        print
        pozostalo = "pozostalo "
        pol = " pol statkow "
        print "  ruch gracza - " + pozostalo + str(self.statki_wroga) + pol + "przeciwnika"
        self.napis("gracz")
        print
        print "  ruch przeciwnika - " + pozostalo + str(self.statki_gracza) + pol + "gracza"
        self.napis("wrog")
